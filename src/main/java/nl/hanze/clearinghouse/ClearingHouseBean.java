package nl.hanze.clearinghouse;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import nl.hanze.cjibbank.CjibBankBean;
import nl.hanze.javabank.JavaBankBean;

/**
 * ClearingHouse EJB. We have no state and the transaction managed should be
 * done at the Bean level instead of the container level.
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class ClearingHouseBean
{
	// The session context of this EJB.
	@Resource
	private SessionContext sessionContext;
	// The EJB handling the CJIB bank transfer.
	@EJB
	private CjibBankBean cjibBankBean;
	// The EJB handling the Java bank transfer.
	@EJB
	private JavaBankBean javaBankBean;

	/**
	 * Transfer money from the source account to the destination.
	 * @param srcAccountNumber
	 * @param dstAccountNumber
	 * @param amount
	 */
	public void transfer(String srcAccountNumber, String dstAccountNumber, float amount)
	{
		UserTransaction tx = sessionContext.getUserTransaction();
		try
		{
			tx.begin();
			cjibBankBean.transfer(amount, srcAccountNumber, dstAccountNumber);
			javaBankBean.transfer(amount, srcAccountNumber, dstAccountNumber);
			tx.commit();
		}
		catch (Exception e)
		{
			rollback(tx);
			throw new RuntimeException(e);
		}
	}

	/**
	 * Roll back a UserTransaction.
	 * 
	 * @param tx
	 */
	private void rollback(UserTransaction tx)
	{
		try
		{
			tx.rollback();
		}
		catch (IllegalStateException | SecurityException | SystemException e1)
		{
			e1.printStackTrace();
			throw new RuntimeException(e1.getMessage(), e1);
		}
	}

}
