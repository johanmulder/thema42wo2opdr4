package nl.hanze.cjibbank;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * The Transaction class of the CJIBBank system. The actual column definitions and
 * such are stored in the parent class.
 * 
 * @author Johan Mulder
 * 
 */
@Entity(name = "CJIBTransaction")
@Table(name = "transaction")
@SuppressWarnings("serial")
public class Transaction extends nl.hanze.bank.Transaction implements Serializable
{
}
