package nl.hanze.clearinghouse.webservice;

import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import nl.hanze.clearinghouse.ClearingHouseBean;

@WebService(serviceName = "ClearingHouse")
public class ClearingHouse
{
	@EJB
	private ClearingHouseBean clearingHouseBean;

	/**
	 * 
	 * @param srcAccountNumber
	 * @param dstAccountNumber
	 * @param amount
	 * @return
	 * @throws Exception
	 */
	@WebMethod
	public boolean transfer(
			@WebParam(name = "srcAccountNumber") String srcAccountNumber,
			@WebParam(name = "dstAccountNumber") String dstAccountNumber,
			@WebParam(name = "amount") float amount) throws Exception
	{
		try
		{
			clearingHouseBean.transfer(srcAccountNumber, dstAccountNumber, amount);
			return true;
		}
		catch (EJBException e)
		{
			if (e.getCause() instanceof Exception)
				throw (Exception) e.getCause();
			throw new RuntimeException(e);
		}
	}
}
