package nl.hanze.javabank;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlType;

/**
 * The Account class of the JavaBank system. The actual column definitions and
 * such are stored in the parent class.
 * 
 * @author Johan Mulder
 * 
 */
@Entity(name = "JavaBankAccount")
@SuppressWarnings("serial")
@XmlType(name = "JavaBankAccount", namespace = "http://webservice.javabank.hanze.nl/account")
@Table(name = "account")
public class Account extends nl.hanze.bank.Account implements Serializable
{
}
