package nl.hanze.bank;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
// The inheritance should be set to table per class, as the entitymanager will
// consider subclasses separate tables otherwise. The account tables are spread
// over multiple databases.
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Transaction
{
	@Id
	// In order to use auto increment fields in MySQL, the Generation type needs
	// to be set to IDENTITY as per http://stackoverflow.com/a/4103347/578745
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "transaction_id")
	protected long id;
	@Column(name = "date", nullable = false)
	@Temporal(TemporalType.DATE)
	protected Date date;
	@Column(name = "src_account", nullable = false)
	protected String srcAccount;
	@Column(name = "dst_account", nullable = false)
	protected String dstAccount;
	@Column(name = "amount")
	protected float amount;

	public Date getDate()
	{
		return date;
	}

	public void setDate(Date date)
	{
		this.date = date;
	}

	public float getAmount()
	{
		return amount;
	}

	public void setAmount(float amount)
	{
		this.amount = amount;
	}

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public String getSrcAccount()
	{
		return srcAccount;
	}

	public void setSrcAccount(String srcAccount)
	{
		this.srcAccount = srcAccount;
	}

	public String getDstAccount()
	{
		return dstAccount;
	}

	public void setDstAccount(String dstAccount)
	{
		this.dstAccount = dstAccount;
	}

}
