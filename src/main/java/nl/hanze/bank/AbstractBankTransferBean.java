package nl.hanze.bank;

import java.util.Date;

import javax.persistence.EntityManager;

/**
 * Abstract bank transfer bean implementing the actual functionality for
 * transferring money between bank accounts.
 * 
 * @author Johan Mulder
 * 
 */
public abstract class AbstractBankTransferBean
{
	/**
	 * Get the local entity manager.
	 * 
	 * @return
	 */
	protected abstract EntityManager getEntityManager();

	/**
	 * Abstract method for transferring money.
	 * 
	 * @param amount
	 * @param src
	 * @param dst
	 */
	public abstract void transfer(float amount, String src, String dst);

	/**
	 * Transfer an amount of money from the source to destination.
	 * 
	 * @param amount
	 * @param src
	 * @param dst
	 * @param accountClass The account class.
	 * @param transactionClass The transaction class.
	 */
	protected void transfer(float amount, String src, String dst,
			Class<? extends Account> accountClass, Class<? extends Transaction> transactionClass)
	{
		// Check if the bank account exists.
		Account srcAccount = getEntityManager().find(accountClass, src);
		Account dstAccount = getEntityManager().find(accountClass, dst);
		// If none of the accounts exist, bail out.
		if (srcAccount == null && dstAccount == null)
			throw new RuntimeException("No bank account found with number " + src + " or " + dst);
		// Check if the source or destination account is in this bank.
		if (srcAccount != null)
		{
			// Subtract money if the source account is local.
			transferFromAccount(srcAccount, dst, amount);
			// Create a transaction.
			createTransaction(src, dst, amount, transactionClass);
		}
		else if (dstAccount != null)
		{
			// Add money if the destination account is local.
			transferToAccount(dstAccount, src, amount);
			createTransaction(src, dst, amount, transactionClass);
		}
	}

	/**
	 * Transfer money from an account.
	 * 
	 * @param src
	 * @param dst
	 * @param amount
	 */
	protected void transferFromAccount(Account src, String dst, float amount)
	{
		// Check if there is enough money available in the bank account.
		float newBalance = src.getBalance() - amount;
		if (newBalance < 0)
			throw new RuntimeException("Not enough money in bank account " + src.getAccountNumber());
		// Update the account.
		src.setBalance(newBalance);
	}

	/**
	 * Tranfer money to an account.
	 * 
	 * @param dst
	 * @param src
	 * @param amount
	 */
	protected void transferToAccount(Account dst, String src, float amount)
	{
		dst.setBalance(dst.getBalance() + amount);
	}

	/**
	 * Create a new transaction log entry.
	 * 
	 * @param src
	 * @param dst
	 * @param amount
	 * @param transactionClass
	 */
	private void createTransaction(String src, String dst, float amount,
			Class<? extends Transaction> transactionClass)
	{
		try
		{
			// Create a new transaction instance.
			Transaction tx = (Transaction) transactionClass.newInstance();
			tx.setAmount(amount);
			tx.setDate(new Date());
			tx.setDstAccount(dst);
			tx.setSrcAccount(src);
			getEntityManager().persist(tx);
		}
		catch (InstantiationException | IllegalAccessException e)
		{
			// This is not likely to happen, unless a subclass of
			// nl.hanze.bank.Account is misconfigured.
			e.printStackTrace();
		}
	}
}
