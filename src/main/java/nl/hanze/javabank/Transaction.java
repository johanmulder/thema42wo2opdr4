package nl.hanze.javabank;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlType;

/**
 * The Transaction class of the JavaBank system. The actual column definitions and
 * such are stored in the parent class.
 * 
 * @author Johan Mulder
 * 
 */
@Entity(name = "javaBankTransaction")
@Table(name = "transaction")
@SuppressWarnings("serial")
@XmlType(name = "JavaBankTransaction", namespace = "http://webservice.javabank.hanze.nl")
public class Transaction extends nl.hanze.bank.Transaction implements Serializable
{
}
