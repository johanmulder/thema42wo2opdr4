package nl.hanze.javabank;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import nl.hanze.bank.AbstractBankTransferBean;

@Stateless
public class JavaBankBean extends AbstractBankTransferBean
{
	@PersistenceContext(unitName = "JavaBank")
	private EntityManager em;

	/*
	 * (non-Javadoc)
	 * @see nl.hanze.bank.AbstractBankTransferBean#getEntityManager()
	 */
	protected EntityManager getEntityManager()
	{
		return em;
	}

	/*
	 * (non-Javadoc)
	 * @see nl.hanze.bank.AbstractBankTransferBean#transfer(float, java.lang.String, java.lang.String)
	 */
	public void transfer(float amount, String src, String dst)
	{
		transfer(amount, src, dst, Account.class, Transaction.class);
	}
}
