package nl.hanze.cjibbank;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * The Account class of the CJIBBank system. The actual column definitions and
 * such are stored in the parent class.
 * 
 * @author Johan Mulder
 * 
 */
// We need a different name for this entity, because otherwise we'd have a duplicate entity.
@Entity(name = "CJIBAccount")
@SuppressWarnings("serial")
@Table(name = "account")
public class Account extends nl.hanze.bank.Account implements Serializable
{
}
