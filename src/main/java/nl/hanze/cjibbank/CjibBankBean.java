package nl.hanze.cjibbank;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import nl.hanze.bank.AbstractBankTransferBean;

@Stateless
public class CjibBankBean extends AbstractBankTransferBean
{
	@PersistenceContext(unitName = "CJIBBank")
	private EntityManager em;
	
	/**
	 * Get the entity manager for this bean,.
	 * @return
	 */
	protected EntityManager getEntityManager()
	{
		return em;
	}
	
	public void transfer(float amount, String src, String dst)
	{
		transfer(amount, src, dst, Account.class, Transaction.class);
	}
}
