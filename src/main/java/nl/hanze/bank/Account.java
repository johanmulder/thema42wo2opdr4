package nl.hanze.bank;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

@Entity
// The inheritance should be set to table per class, as the entitymanager will
// consider subclasses separate tables otherwise. The account tables are spread
// over multiple databases.
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class Account
{
	@Id
	@Column(name = "account_number", nullable = false)
	protected String accountNumber;
	@Column(name = "name", nullable = false)
	protected String name;
	@Column(name = "address", nullable = false)
	protected String address;
	@Column(name = "city", nullable = false)
	protected String city;
	@Column(name = "`limit`", nullable = false)
	protected float limit = 0;
	@Column(name = "balance", nullable = false)
	protected float balance;

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getAddress()
	{
		return address;
	}

	public void setAddress(String address)
	{
		this.address = address;
	}

	public String getCity()
	{
		return city;
	}

	public void setCity(String city)
	{
		this.city = city;
	}

	public float getLimit()
	{
		return limit;
	}

	public void setLimit(float limit)
	{
		this.limit = limit;
	}

	public float getBalance()
	{
		return balance;
	}

	public void setBalance(float balance)
	{
		this.balance = balance;
	}

	public String getAccountNumber()
	{
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber)
	{
		this.accountNumber = accountNumber;
	}
}
